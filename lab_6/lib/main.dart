import 'dart:html';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    const appTitle = 'Welcome to Reminder!';
    const title1 = 'Login';
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text(title1, style: TextStyle(color: Colors.white)),
        ),
        body: const MyHomePageForm(),
      ),
    );
  }
}

class MyHomePageForm extends StatefulWidget {
  const MyHomePageForm({Key? key}) : super(key: key);

  MyHomePage createState() => MyHomePage();
}

class MyHomePage extends State<MyHomePageForm> {
  final _formkey = GlobalKey<FormState>();

  bool _obscureText = true;
  String _password = '';

  void toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
          Widget>[
        Text('\n'),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 25),
          child: TextFormField(
            autofocus: true,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Username',
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "Username field cannot be empty!";
              }
              return null;
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 25),
          child: TextFormField(
            autofocus: true,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Password',
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "Password field cannot be empty!";
              } else if (value.length < 8) {
                return 'Password too short.';
              }
              return null;
            },
            //onSaved: (value) => _password = value!,
            obscureText: _obscureText,
          ),
        ),
        Padding(
            padding: EdgeInsets.fromLTRB(1300, 20, 20, 10),
            child: ElevatedButton(
              onPressed: toggle,
              child: new Text(_obscureText ? "Show Password" : "Hide Password"),
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.blueAccent),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
            )),
        Padding(
          // padding: EdgeInsets.symmetric(horizontal: 700, vertical: 50),
          // child: SizedBox(
          //   height: 40,
          //   width: 100,
          //   child: TextButton(
          //     style: ButtonStyle(
          //       backgroundColor:
          //           MaterialStateProperty.all<Color>(Colors.blueAccent),
          //       foregroundColor:
          //           MaterialStateProperty.all<Color>(Colors.white),
          //     ),
          //     onPressed: () {},
          //     child: Text('Login'),
          //   ),
          // )
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.blueAccent),
              onPressed: () {
                // if (_formkey.currentState!.validate()) {
                ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Logging in ...')));
              },
              // },
              child: const Text(
                'Login',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
