```
Michael Wong
2006528080
PBP - D (ARL)
Lab 2
```
# Soal Lab2
1. Apakah perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?

# Jawaban:
1. JSON dan XML merupakan kedua hal yang berbeda. JSON (_JavaScript Object Notation_) merupakan format pertukaran data yang memudahkan komputer untuk compiling data yang ingin dikirim, sedangkan XML (_eXtensible Markup Language_) merupakan bahasa markup yang digunakan untuk pertukaran data dari satu aplikasi ke aplikasi lain melalui internet. Dan dari segi yang lain, *JSON* lebih mudah dibaca oleh manusia daripada dokumen dalam bentuk XML. Namun, XML juga diperlukan untuk kebutuhan markup dokumen dan informasi metadata.

2. XML dan HTML keduanya merupakan bahasa markup. Namun, kedua bahasa markup tersebut secara umum memiliki fungsi yang cukup berbeda. XML lebih berfungsi untuk menciptakan dan mengirimkan sebuah data/informasi, sedangkan HTML (_HyperText Markup Language_) lebih berfungsi untuk mengatur penyajian tampilan data. Lebih lanjut, XML dan HTML memiliki perbedaan dalam *tag*, dimana HTML memiliki *tag* yang sudah diatur _HTML Element_ untuk tampilan HTML dengan fungsinya masing-masing (contoh: <h1>Headings 1</h1>, <p>Paragraph</p>, dsb.). Sedangkan, XML memiliki *tag* yang dapat di-customize sesuai keperluan informasi/data.