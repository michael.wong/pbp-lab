from django.http import response
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from lab_3.forms import FriendForm 

# Login required for index and add_friend
@login_required(login_url='/admin/login/')
def index(request): 
    friends = Friend.objects.all()  
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm()

    if request.method == "POST":
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return response.HttpResponseRedirect("/lab-3")

    return render(request, 'lab3_form.html', {'form':form})