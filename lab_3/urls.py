from django.urls import path
from .views import index, add_friend
from lab_1.views import friend_list

urlpatterns = [
    path('', index, name='index'),
    path('friends', friend_list, name = 'friend'),
    # Implement add_friend Views
    path('add', add_friend, name='add_friend')
]
